from django.contrib import admin
from .models import Becas, Universidad, Pais

# Register your models here.
admin.site.register(Universidad)
admin.site.register(Pais)
admin.site.register(Becas)
