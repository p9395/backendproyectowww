from django.db import models

# Create your models here.

class Universidad(models.Model):
    nombre  = models.CharField(max_length=50)
    nit = models.IntegerField()

    def __str__(self) -> str:
        return self.nombre

class Pais(models.Model):
    nombre  = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.nombre

class Becas(models.Model):
    CATEGORIAS_BECAS = (
        ('M', 'Maestrías'),
        ('D', 'Doctorados'),
    )
    nombre  = models.CharField(max_length=50)
    categoria = models.CharField(max_length=1, choices=CATEGORIAS_BECAS)
    porcentaje = models.IntegerField()
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    universidad =  models.ForeignKey(Universidad, on_delete=models.CASCADE)
    requerimientos = models.CharField(max_length=1500)
    internacional = models.BooleanField()
    principalPage = models.BooleanField()

    def __str__(self) -> str:
        return self.nombre

