from django.urls import path
from .views import BecasView, PaisView, UniversidadView

urlpatterns = [
    path("", BecasView.as_view(), name="becas"),
    path("beca/<int:id>", BecasView.as_view(), name="becas"),
    path("paises", PaisView.as_view(), name="pais"),
    path("universidades", UniversidadView.as_view(), name="universidad"),
]