from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from .models import Becas, Pais, Universidad
import json

# Create your views here.
class BecasView(View):
    def changeToJson(data):
        return {
            "id": data.id,
            "nombre": data.nombre,
            "categoria": data.categoria,
            "porcentaje": data.porcentaje,
            "pais": {
                "id": data.pais.id,
                "nombre": data.pais.nombre,
            },
            "universidad": {
                "id": data.universidad.id,
                "nombre": data.universidad.nombre,
                "nit": data.universidad.nit,
            },
            "requerimientos": data.requerimientos,
            "internacional": data.internacional,
            "principalPage": data.principalPage
        }

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        id
        if(id > 0):
            try:
                beca = list(Becas.objects.filter(id=id))
                return JsonResponse({'response': BecasView.changeToJson(beca[0])})
            except:
                return JsonResponse({'Error': "Error Server"})
        else:
            try:
                response = []
                becas = list(Becas.objects.all())
                for beca in becas:
                    response.append(BecasView.changeToJson(beca))
                return JsonResponse({'response': response})
            except:
                return JsonResponse({'Error': "Error Server"})

    def post(self, request):
        data = json.loads(request.body)

        try:
            pais = Pais.objects.get(id=data["pais_id"])
        except Pais.DoesNotExist:
            return JsonResponse({'Error': '''El pais seleccionado no existe,
            por favor selecione algun pais existente o cree uno nuevo'''})

        try:
            universidad = Universidad.objects.get(id=data["universidad_id"])
        except Universidad.DoesNotExist:
            return JsonResponse({'Error': '''La universidad seleccionada no existe.
            Por favor selecione alguna universidad existente o cree uno nuevo'''})

        try:
            newBeca = Becas.objects.create(
                nombre=data["nombre"],
                categoria=data["categoria"],
                porcentaje=data["porcentaje"],
                pais=pais,
                universidad=universidad,
                requerimientos=data["requerimientos"],
                internacional=data["internacional"],
                principalPage=data["principalPage"]
            )
            return JsonResponse({"response": BecasView.changeToJson(newBeca)})
        except:
            return JsonResponse({'Error': "Error Server"})

    def put(self, request, id=0):
        if(id > 0):
            data = json.loads(request.body)

            try:
                beca = Becas.objects.get(id=id)
            except Universidad.DoesNotExist:
                return JsonResponse({'Error': 'La beca seleccionada no existe.'})
            else:
                try:
                    pais = Pais.objects.get(id=data["pais_id"])
                except Pais.DoesNotExist:
                    return JsonResponse({'Error': '''El pais seleccionado no existe,
                    por favor selecione algun pais existente o cree uno nuevo'''})

                try:
                    universidad = Universidad.objects.get(
                        id=data["universidad_id"])
                except Universidad.DoesNotExist:
                    return JsonResponse({'Error': '''La universidad seleccionada no existe.
                    Por favor selecione alguna universidad existente o cree uno nuevo'''})

                beca.nombre = data['nombre']
                beca.categoria = data['categoria']
                beca.porcentaje = data['porcentaje']
                beca.pais = pais
                beca.universidad = universidad
                beca.requerimientos = data['requerimientos']
                beca.internacional = data['internacional']
                beca.principalPage = data['principalPage']
                beca.save()

            return JsonResponse({"response": BecasView.changeToJson(beca)})

    def delete(self, request, id):
        try:
            beca = Becas.objects.get(id=id)
            nombre = beca.nombre
        except Pais.DoesNotExist:
            return JsonResponse({'Error': 'Le beca a borrar no existe'})

        beca.delete()
        return JsonResponse({'response': "La beca %s fue eliminada." % (nombre)})

class PaisView(View):
    def changeToJson(data):
        return {
            "id": data.id,
            "nombre": data.nombre,
        }

    def get(self, request):
        try:
            response = []
            paises = list(Pais.objects.all())
            for pais in paises:
                response.append(PaisView.changeToJson(pais))

            return JsonResponse({'response': response})
        except:
            return JsonResponse({'Error': "Error Server"})

class UniversidadView(View):
    def changeToJson(data):
        return {
                "id": data.id,
                "nombre": data.nombre,
                "nit": data.nit,
        }

    def get(self, request):
        try:
            response = []
            universidades = list(Universidad.objects.all())
            for universidad in universidades:
                response.append(UniversidadView.changeToJson(universidad))

            return JsonResponse({'response': response})
        except:
            return JsonResponse({'Error': "Error Server"})
